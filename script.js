document.querySelectorAll('a[href^="#"]').forEach(anchor =>{
    anchor.addEventListener("click", function(e){
        e.preventDefault();
        document.querySelector(this.getAttribute("href")).scrollIntoView({
            behavior : "smooth"
        });
    });
});


let popup = document.getElementById("popup");
let btn = document.getElementById("contact");

function openpopup()
{
    btn.classList.add("hidebtn");
    popup.classList.add("openpopup");
}

function closepopup()
{
    popup.classList.remove("openpopup");
    btn.classList.remove("hidebtn");
    
}


