<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CV Tanguy Vienot</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="styles.css">  
</head>
<body>
    
    <br>
    <header>
        <img id="logo" src="profile.jpg" alt="profile picture" width = "120px" height = "120px">
        <nav>
            <ul class = "menu">
                <li><a href="#a">Formations</a></li>
                <li><a href="#b">Expériences pro</a></li>
                <li><a href="#c">Compétences</a></li>
                <li><a href="#d">Centres d'intérêts</a></li>
            </ul>

        </nav>
        <div>
            <button class="btn" id="contact" type="submit" onclick="openpopup()">Contact</button>
        </div>
    </header>
    <div class="popup" id="popup">
        <h1>Contact</h1>
        <form class="sendmsg" action="sendmsg.php" method="post">
            <textarea class="ta" name="message" ></textarea>
            <br>
            <button style="margin-top : 10px;" class="btn"  name="submit" type="submit" onclick="closepopup()">Envoyer</button>
        </form>
        <button style="margin-top : 10px;" class="btn" onclick="closepopup()">Annuler</button>
        
        
    </div>
    <main class="wrapper">
        
        <article id="a">
            <h1><img id="imgg" src="graduation-hat.png" title="book icons" width="25px">Formations</h1>
            <section id="left">
            <table>
                <tr><td>
                    <h4>2021</h4>
                    <h5>Londres</h5>
                    <br>
                </td></tr>
                <tr><td>
                    <h4>2019 - 2024</h4>
                    <h5>Paris 75015</h5>
                    <br>
                </td></tr>
                <tr><td>
                    <h4>2010 - 2019</h4>
                    <h5>Le Vésinet 78110</h5>
                </td></tr>
            </table>
            </section>
            <section id="right">
                    <table>
                        <tr><td>
                            <h4>Semestre à l'étranger</h4>
                            <h5>INSEEC.U London</h5>
                            <br>
                        </td></tr>
                        <tr><td>
                            <h4>Ecole d’ingénieur – Majeure Objets connectés, réseaux et services</h4>
                            <h5>ECE Paris</h5>
                            <br>
                        </td></tr>
                        <tr><td>
                            <h4>Collège et Lycée – Baccalauréat scientifique avec mention</h4>
                            <h5>Lycée le Bon Sauveur</h5>
                        </td></tr>
                    </table>
            </section>
        </article>

        <article id="b">
            <h1><img id="imgg" src="flexible.png" title="book icons" width="25px">Expériences professionnelles</h1>
            <section id="left">
                <table>
                    <tr><td>
                        <h4>Dec/Jan 21/22</h4>
                        <h5>Evreux 27000</h5>
                        <br>
                    </td></tr>
                    <tr><td>
                        <h4>Dec/Jan 20/21</h4>
                        <h5>Evreux 27000</h5>
                        <br>
                    </td></tr>
                    <tr><td>
                        <h4>Juin/Aout 2020</h4>
                        <h5>Rueil-Malmaison 92500</h5>
                        <br>
                    </td></tr>
                    <tr><td>
                        <h4>Juin 2016</h4>
                        <h5>Evreux 27000</h5>
                        <br>
                    </td></tr>
                    <tr><td>
                        <h4>Décembre 2015</h4>
                        <h5>Nanterre 92000</h5>
                        <br>
                    </td></tr>
                </table>
            </section>
            <section id="right">
                    <table>
                        <tr><td>
                            <a href="https://jeulin.com/jeulin_fr/">
                                <h4>JEULIN </h4>
                                <h5>Stage en développement informatique</h5>
                                <p>Développement de logiciels au bureau d’étude de l’entreprise. (C, C++, Python, HTML, JS)</p>
                                <br>
                            </a>
                        </td></tr>
                        <tr><td>
                            <a href="https://jeulin.com/jeulin_fr/">
                                <h4>JEULIN</h4>
                                <h5>Stage en développement informatique</h5>
                                <p>Développement de logiciels au bureau d’étude de l’entreprise. (C, C++, Python)</p>
                                <br>
                            </a>
                        </td></tr>
                        <tr><td>
                            <a href="">
                                <h4>Leroy Merlin</h4>
                                <h5>Hôte service client</h5>
                                <p>Chargé de gérer les caisses automatiques et le flux de clients aux caisses.
                                    Caissier aux caisses traditionnelles.</p>
                                <br>
                            </a>
                        </td></tr>
                        <tr><td>
                            <a href="https://jeulin.com/jeulin_fr/">
                                <h4>JEULIN</h4>
                                <h5>Monteur : Stage en entreprise de 2nd</h5>
                                <p>Travail dans l’atelier de montage, soudure et montage du matériel envoyé aux écoles pour leurs laboratoires de sciences.</p>
                                <br>
                            </a>
                        </td></tr>
                        <tr><td>
                            <a href="">
                                <h4>Boulangerie la Campagnarde</h4>
                                <h5>Stage d’observation de 3eme</h5>
                                <p>Stage encadré par le tourier
                                    Réalisation de viennoiseries.</p>
                                <br>
                            </a>
                        </td></tr>
                    </table>
            </section>
        </article>   

        <article id="c">
            <h1><img id="imgg" src="League-of-Legends-Game-Logo.jpg" title="book icons" width="25px">Compétences</h1>
            <section id="left">
                <table>
                    <tr>
                        <td>
                            <h4>Informatique</h4>
                        </td>
                    </tr>
                </table>
            </section>
            <section class="carousel" id="right">
                <table>
                    <tr>
                        <td>
                            <div class="hidden">
                                <div class="contenant">
                                    <p class="element">C</p>
                                    <p class="element">C++</p>
                                    <p class="element">Python</p>
                                    <p class="element">VB</p>
                                    <p class="element">SQL</p>
                                    <p class="element">JAVA</p>
                                    <p class="element">HTML</p>
                                    <p class="element">JS</p>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </section>
        </article>

        <article id="d">
            <h1><img id="imgg" src="League-of-Legends-Game-Logo.jpg" title="book icons" width="25px">Centres d'intérêts</h1>
            <section id="left">
                <h4>Boxe Anglaise</h4>
            </section>
            <section id="right">
                <center>
                    <iframe width="392" height="220" src="https://www.youtube.com/embed/ihw9cnljKh4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" ></iframe>
                </center>
            </section>
        </article>

    </main>
    <footer>

    </footer>
    <script src="script.js"></script>
</body>
</html>